//
//  LocationViewModel.swift
//  BeaconDistanceIndicator
//
//  Created by MIKHAIL RAKHMANOV on 30.03.16.
//  Copyright © 2016 No Logo. All rights reserved.
//


import Foundation
import UIKit
import CoreLocation
import CoreMotion
import ReactiveCocoa


class LocationModel: NSObject {
	
	let totalSquaresInView: Int
	
	let rectangleSize: CGSize
	
	let currentBeacon = Beacon (name: "MyBeacon", uuid: NSUUID (UUIDString: "A07C5CA8-59EB-4EA8-9956-30B776E0FEDC")!, majorValue: 3, minorValue: 3)
	
	let locationManager = CLLocationManager ()
	let motionManager = CMMotionManager ()
	let gyroQueue = NSOperationQueue.mainQueue()
	
	let maximumDistance = 4.8
	let nearDistance = 0.7
	let farDistance = 3.0
	let veryFarDistance = 3.8
	let referenceAcceleration = CMAcceleration (x: 2.0, y: 2.0, z: 2.0)
	var percentNear = MutableProperty (0)
	
	var movementDisposable: Disposable?
	
	init (total: Int) {
		
		totalSquaresInView = total
		rectangleSize      = CGSizeMake (UIViewController.screenWidth (), UIViewController.screenHeight() / CGFloat (total))
		
		super.init ()
		
		locationManager.requestAlwaysAuthorization ()
		locationManager.delegate = self
		
		initialiseCurrentBeacon ()
	}
	
	deinit {
		deinitialiseCurrentBeacon ()
		motionManager.stopGyroUpdates ()
	}
	
	func getColorForSquareNo (number: Int) -> UIColor { // red, yellow, green palette
		
		let hue = CGFloat (totalSquaresInView - number) / CGFloat (totalSquaresInView) * 0.20
		
		return UIColor (hue: hue, saturation: 1.0, brightness: 1.0, alpha: 1.0)
	}
	
	func initialiseCurrentBeacon () {
		let beaconRegion = CLBeaconRegion (proximityUUID: currentBeacon.uuid, major: currentBeacon.majorValue, minor: currentBeacon.minorValue, identifier: currentBeacon.name)
		
		locationManager.startMonitoringForRegion (beaconRegion)
		locationManager.startRangingBeaconsInRegion (beaconRegion)
	}
	
	func deinitialiseCurrentBeacon () {
		let beaconRegion = CLBeaconRegion (proximityUUID: currentBeacon.uuid, major: currentBeacon.majorValue, minor: currentBeacon.minorValue, identifier: currentBeacon.name)
		
		locationManager.stopMonitoringForRegion (beaconRegion)
		locationManager.stopRangingBeaconsInRegion (beaconRegion)
	}
	
	func checkMotionAndComplete (completion: () -> ()) {
		
		if motionManager.deviceMotionAvailable {
			motionManager.deviceMotionUpdateInterval = 0.02
			motionManager.startDeviceMotionUpdatesToQueue (gyroQueue) {
				[weak self] data, error in
				
				guard let data = data, let _self = self where error == nil
					else {
						print ("Not able to receive motion data")
						return
				}
				
				if data.userAcceleration > _self.referenceAcceleration {
					completion ()
					_self.deinitialiseMotionManager()
				}
			}
		}
	}
	
	func deinitialiseMotionManager () {
		//motionManager.stopDeviceMotionUpdates ()
	}
	
	func movementUpdateSignalProducer () -> SignalProducer <Void, NoError> {
		
		//let movementDisposable =
		
		return SignalProducer { [weak self] observer, disposable in
			
			guard self!.motionManager.deviceMotionAvailable
				else {
					observer.sendCompleted()
					return
			}
			
			self!.motionManager.startDeviceMotionUpdatesToQueue (self!.gyroQueue) {
				[weak self] data, error in
				
				guard let data = data, let _self = self where error == nil
					else {
						print ("Not able to receive motion data")
						return
				}
				
				if fabs (data.userAcceleration.x) > 0.12 || fabs (data.userAcceleration.y) > 0.12 {
					observer.sendNext()
				}
			}
		}
	}
}

extension LocationModel: CLLocationManagerDelegate {
	func locationManager(manager: CLLocationManager, monitoringDidFailForRegion region: CLRegion?, withError error: NSError) {
		
		print("Failed monitoring region: \(error.description)")
	}
 
	func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
		print("Location manager failed: \(error.description)")
	}
	
	func locationManager(manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], inRegion region: CLBeaconRegion) {
		for beacon in beacons {
			if currentBeacon == beacon {
				
				var accuracy = Double (beacon.accuracy)
				if accuracy < nearDistance {
					accuracy /= 1.05
				}
				if accuracy > farDistance {
					accuracy *= 1.1
				}
				let proportion = (veryFarDistance - accuracy) / veryFarDistance
				
				let intProportion = Int (proportion * 100) < 5 && accuracy < maximumDistance ? 5 : Int (proportion * 100)
				
				percentNear.value = intProportion
			}
		}
	}
}