//
//  ViewController.swift
//
//  Created by MIKHAIL RAKHMANOV on 13.03.16.
//  Copyright © 2016 No Logo. All rights reserved.
//

import UIKit
import AVFoundation
import CoreLocation
import ReactiveCocoa

// MARK: ReactiveCocoa

import enum Result.NoError
public typealias NoError = Result.NoError

class ViewController: UIViewController {
	
	let beepSoundUrl = NSURL (fileURLWithPath: NSBundle.mainBundle().pathForResource("beep", ofType: "wav")!)
	let waveSoundUrl = NSURL (fileURLWithPath: NSBundle.mainBundle().pathForResource("wave", ofType: "wav")!)
	
	@IBOutlet weak var informationLabel: UILabel!
	@IBOutlet weak var filterView: UIView!
	@IBOutlet weak var resetButton: UIButton!
	@IBOutlet weak var indicatorView: UIView! // main black view
	
	@IBOutlet weak var percentageLabel: UILabel!
	var indicatorBars: [UIView] = [] // views representing indicator bars
	
	var model: LocationModel = LocationModel (total: 60) // total amount of colored rectangles
	
	var audioPlayer = AVAudioPlayer ()
	var sineAudioPlayer = AVAudioPlayer ()
	
	let beepTimerInterval = 0.1
	
	var percentageLeft = 0
	
	var started = false
	var delayActivated = false
	
	var timerDisposable: Disposable?
	var motionUpdatesDisposable: Disposable?
	var indicatorDisposable: Disposable?
	
	override func viewDidLoad() {
		super.viewDidLoad()

		drawAllRectanglesAndHide ()
		startModel ()
	}
	
	func startModel () {
		
		model.checkMotionAndComplete (startAction)
	}
	
	func startAction () {
		
		guard !started
			else {
				return
		}
		
		do {
			audioPlayer = try AVAudioPlayer (contentsOfURL: beepSoundUrl)
			audioPlayer.prepareToPlay ()
			sineAudioPlayer = try AVAudioPlayer (contentsOfURL: waveSoundUrl)
			sineAudioPlayer.prepareToPlay ()
		} catch {
			print ("Error playing audio file")
		}
		
		audioPlayer.volume = 1.0
		
		started = true
		
		timerDisposable = timerSignalProducer().startWithNext { [weak self] cycle in
			
			// determining frequency of beeps based on the distance between the beacon and the receiver
			switch self!.model.percentNear.value {
			case 0...15:
				break
			case 16...44:
				if cycle % 8 == 0 {
					self!.playBeepSound ()
				}
			case 45...75:
				if cycle % 4 == 0 {
					self!.playBeepSound ()
				}
			case 76...100:
				self!.playSineWave ()
			default:
				break
			}
			
			self!.showPercentageScanningAnalysis ()
		}
		
		motionUpdatesDisposable = model.movementUpdateSignalProducer()
			.startWithNext { [weak self] _ in
			self!.percentageLeft = 100
				
			self!.informationLabel.hidden = false
			self!.percentageLabel.hidden = false
		}
	
		indicatorDisposable = model.percentNear.producer
			.startWithNext { [weak self] percent in
				guard percent <= 100
					else {
						return
				}
				
				self!.showPercentageByRectangles (percent)
		}
	}
	
	func showPercentageScanningAnalysis () {
		if percentageLeft > 0 {
			percentageLeft -= 2
			dispatch_async(dispatch_get_main_queue()) { [weak self] in
				self!.percentageLabel.text = String (self!.percentageLeft) + "%"
			}
			
			informationLabel.text = "Идет сканирование, пожалуйста, не двигайтесь"
		} else {
			percentageLabel.hidden = true
			informationLabel.hidden = true
		}
	}
	
	func playBeepSound ()
	{
		if sineAudioPlayer.playing {
			sineAudioPlayer.stop ()
		}
		
		do {
			audioPlayer = try AVAudioPlayer (contentsOfURL: beepSoundUrl)
			audioPlayer.prepareToPlay ()
			audioPlayer.play ()
		} catch {
			print("Error getting the audio file")
		}
	}
	
	func playSineWave () {
		
		guard !sineAudioPlayer.playing
			else {
				return
		}
		
		if audioPlayer.playing {
			audioPlayer.stop ()
		}
		
		sineAudioPlayer.play ()
	}
	
	func timerSignalProducer () -> SignalProducer <Int, NoError> {
		
		return SignalProducer { [weak self] observer, disposable in
			
			var timesOccured = 0
			
			NSTimer.schedule(repeatInterval: self!.beepTimerInterval
			) { timer in
				observer.sendNext(timesOccured)
				timesOccured += 1
				
				if timesOccured == 24 { // new cycle
					timesOccured = 0
				}
			}
		}
	}
	
	func showPercentageByRectangles (percent: Int) {
		
		let amountOfSquaresShown = percent * model.totalSquaresInView / 100
		
		for index in 0 ..< indicatorBars.count {
			if index < amountOfSquaresShown {
				indicatorBars [index].hidden = false
			} else {
				indicatorBars [index].hidden = true
			}
			filterView.alpha = percentageLeft > 0 ? 0.5 : 0
		}
	}
	
	func drawAllRectanglesAndHide () {
		
		var currentY = UIViewController.screenHeight()
		
		let rectangleSize = model.rectangleSize
		let totalSquares  = model.totalSquaresInView
		
		for index in 0 ..< totalSquares {
			
			let view = UIView ()
			
			currentY -= rectangleSize.height
			
			let origin = CGPointMake (0, currentY)
			view.frame = CGRect (origin: origin, size: rectangleSize)
			view.backgroundColor = model.getColorForSquareNo (index + 1)
			
			indicatorView.addSubview(view)
			
			indicatorBars.append(view)
			
			view.hidden = true
		}
		
		
		view.bringSubviewToFront (percentageLabel)
		view.bringSubviewToFront (informationLabel)
		view.bringSubviewToFront (filterView)
		view.bringSubviewToFront (resetButton)
	}
	
	// MARK: Autorotation disabled
	
	override func shouldAutorotate() -> Bool {
		return false
	}
	
	override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
		return UIInterfaceOrientationMask.Portrait
	}
	
	func reset () {
		
		guard started else {
			return
		}
		
		indicatorDisposable?.dispose ()
		timerDisposable?.dispose ()
		motionUpdatesDisposable?.dispose ()
		
		if audioPlayer.playing {
			audioPlayer.stop ()
		}
		
		if sineAudioPlayer.playing {
			sineAudioPlayer.stop ()
		}
		
		percentageLabel.hidden = true
		informationLabel.hidden = true
		
		showPercentageByRectangles (0)
		started = false
		
		showDelayAlert ()
	}
	
	func showDelayAlert () {
		
		let alertController = UIAlertController (title: "Уведомление", message: "Через 30 секунд после нажатия кнопки \"Ок\" произойдет перезагрузка", preferredStyle: .Alert)
		
		let action = UIAlertAction (title: "Ок", style: .Default) { [weak self] _ in
			
			self?.delayActivated = true
			
			
			delay (30.0) { [weak self] in
				self?.delayActivated = false
				self?.startModel ()
			}
		}
		
		alertController.addAction (action)
		
		presentViewController (alertController, animated: true, completion: nil)
	}
	
	
	@IBAction func resetButtonPressed(sender: AnyObject) {
		
		reset ()
	}
	
	@IBAction func screenTapped(sender: AnyObject) {
		
		guard !delayActivated
			else {
				return
		}
		
		startAction ()
	}
}





